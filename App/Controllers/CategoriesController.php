<?php
namespace App\Controllers;

use App\Models\CategoriesModel;

class CategoriesController
{
    public function readAll()
    {
        // Headers requis
        // Accès depuis n'importe quel site ou appareil (*)
        header("Access-Control-Allow-Origin: *");

        // Format des données envoyées
        header("content-type: application/json; charset=utf-8");

        // Méthode autorisée
        header("Access-Control-Allow-Methods: GET");

        // Durée de vie de la requête
        header("Access-Control-Max-Age: 3600");

        // Entêtes autorisées
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            // La bonne méthode est utilisée
            $categories = new CategoriesModel(); // On récupère les données
            $stmt = $categories->findAll();

            // On vérifie si on a au moins 1 categorie
            if ($stmt->rowCount() > 0) {
                // On initialise un tableau associatif
                $tableaucategories = [];
                $tableaucategories['categories'] = [];

                // On parcourt les categories
                foreach ($stmt->fetchAll() as $objcategorie) {
                    $cat = [
                        "id" => $objcategorie->getId(),
                        "nom" => $objcategorie->getNom(),
                        "description" => $objcategorie->getDescription(),
                        "created_at" => $objcategorie->getCreated_at(),
                        "updated_at" => $objcategorie->getUpdated_at(),
                    ];
                    $tableaucategories['categories'][] = $cat;
                }

                // On envoie le code réponse 200 OK
                http_response_code(200);

                // On encode en json et on envoie
                echo json_encode($tableaucategories);
            }

        } else {
            // Mauvaise méthode, on gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }
    public function readOne(string $name)
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie que la méthode utilisée est correcte
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {

            // On instancie les categories
            $categorieModel = new CategoriesModel();

            // On récupère le categorie
            $stmt = $categorieModel->findAllProductByNameCategorie($name);

            // On vérifie si on a au moins 1 categorie
            if ($stmt->rowCount() > 0) {
                $resultat = $stmt->fetchAll();
                // On initialise un tableau associatif
                $tableaucategories = [];
                $tableaucategories['categories'] = [];
                if (count($resultat) === 1 && $resultat[0]->getProduits_id() === null) {
                    http_response_code(301);
                    echo json_encode(array("message" => "Il n'y a pas de produit dans cette catégorie."));
                } else {
                    // On parcourt les categories
                    foreach ($resultat as $objcategorie) {
                        $cat = [
                            "categorie" => $objcategorie->getNom(),
                            "id" => $objcategorie->getProduits_id(),
                            "nom" => $objcategorie->getProduits_nom(),
                            "description" => $objcategorie->getProduits_desc(),
                            "prix" => $objcategorie->getProduits_prix(),
                        ];
                        $tableaucategories['categories'][] = $cat;
                    }

                    // On envoie le code réponse 200 OK
                    http_response_code(200);

                    // On encode en json et on envoie
                    echo json_encode($tableaucategories);}
            } else {
                // 404 Not found
                http_response_code(404);

                echo json_encode(array("message" => "Le categorie n'existe pas."));
            }

        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }

    public function createProd()
    {
        // Headers requis
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie la méthode
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // On instancie les categories
            $categorie = new CategoriesModel();

            // On récupère les informations envoyées
            $donnees = json_decode(file_get_contents("php://input"));

            if (!empty($donnees->nom) && !empty($donnees->description) && !empty($donnees->prix) && !empty($donnees->categories_id)) {
                // Ici on a reçu les données
                // On hydrate notre objet
                $params = [':nom' => $donnees->nom,
                    ':description' => $donnees->description];

                if ($categorie->create($params)) {
                    // Ici la création a fonctionné
                    // On envoie un code 201
                    http_response_code(201);
                    echo json_encode(["message" => "L'ajout a été effectué"]);
                } else {
                    // Ici la création n'a pas fonctionné
                    // On envoie un code 503
                    http_response_code(503);
                    echo json_encode(["message" => "L'ajout n'a pas été effectué"]);
                }
            }
        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }

    public function updateProd()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: PUT");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie la méthode
        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {

            // On instancie les categories
            $categorie = new CategoriesModel();

            // On récupère les informations envoyées
            $donnees = json_decode(file_get_contents("php://input"));

            $id = "";
            $params = [];
            if (!empty($donnees->id)) {
                $id = $donnees->id;
            };
            if (!empty($donnees->nom)) {$params[':nom'] = $donnees->nom;};
            if (!empty($donnees->description)) {$params[':description'] = $donnees->description;};
            if (!empty($donnees->updated_at)) {$params[':updated_at'] = $donnees->updated_at;};
            if (!empty($id) && !empty($params)) {
                // Ici on a reçu les données
                if ($categorie->update($id, $params)) {
                    // Ici la modification a fonctionné
                    // On envoie un code 200
                    http_response_code(200);
                    echo json_encode(["message" => "La modification a été effectuée"]);
                } else {
                    // Ici la création n'a pas fonctionné
                    // On envoie un code 503
                    http_response_code(503);
                    echo json_encode(["message" => "La modification n'a pas été effectuée"]);
                }
            }
        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }

    public function deleteProd()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: DELETE");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie que la méthode utilisée est correcte
        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {

            // On instancie les categories
            $categorie = new CategoriesModel();

            // On récupère l'id du categorie
            $donnees = json_decode(file_get_contents("php://input"));

            if (!empty($donnees->id)) {
                if ($categorie->delete($donnees->id)) {
                    // Ici la suppression a fonctionné
                    // On envoie un code 200
                    http_response_code(200);
                    echo json_encode(["message" => "La suppression a été effectuée"]);
                } else {
                    // Ici la création n'a pas fonctionné
                    // On envoie un code 503
                    http_response_code(503);
                    echo json_encode(["message" => "La suppression n'a pas été effectuée"]);
                }
            }
        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }
}