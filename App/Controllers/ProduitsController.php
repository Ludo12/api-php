<?php
namespace App\Controllers;

use App\Models\ProduitsModel;

class ProduitsController
{
    public function readAll()
    {
        // Headers requis
        // Accès depuis n'importe quel site ou appareil (*)
        header("Access-Control-Allow-Origin: *");

        // Format des données envoyées
        header("content-type: application/json; charset=utf-8");

        // Méthode autorisée
        header("Access-Control-Allow-Methods: GET");

        // Durée de vie de la requête
        header("Access-Control-Max-Age: 3600");

        // Entêtes autorisées
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            // La bonne méthode est utilisée
            $produits = new ProduitsModel(); // On récupère les données
            $stmt = $produits->findAll();

            // On vérifie si on a au moins 1 produit
            if ($stmt->rowCount() > 0) {
                // On initialise un tableau associatif
                $tableauProduits = [];
                $tableauProduits['produits'] = [];

                // On parcourt les produits
                foreach ($stmt->fetchAll() as $objProduit) {
                    $prod = [
                        "id" => $objProduit->getId(),
                        "nom" => $objProduit->getNom(),
                        "description" => $objProduit->getDescription(),
                        "prix" => $objProduit->getPrix(),
                        "categories_id" => $objProduit->getCategories_id(),
                        "categories_nom" => $objProduit->getCategories_nom(),
                    ];
                    $tableauProduits['produits'][] = $prod;
                }

                // On envoie le code réponse 200 OK
                http_response_code(200);

                // On encode en json et on envoie
                echo json_encode($tableauProduits);
            }

        } else {
            // Mauvaise méthode, on gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }
    public function readOne(int $id)
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie que la méthode utilisée est correcte
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {

            // On instancie les produits
            $produitModel = new ProduitsModel();

            // On récupère le produit
            $produit = $produitModel->findOneById($id);

            // On vérifie si le produit existe
            if ($produit !== null) {

                $prod = [
                    "id" => $produit->getId(),
                    "nom" => $produit->getNom(),
                    "description" => $produit->getDescription(),
                    "prix" => $produit->getPrix(),
                    "categories_id" => $produit->getCategories_id(),
                    "categories_nom" => $produit->getCategories_nom(),
                ];
                // On envoie le code réponse 200 OK
                http_response_code(200);

                // On encode en json et on envoie
                echo json_encode($prod);
            } else {
                // 404 Not found
                http_response_code(404);

                echo json_encode(array("message" => "Le produit n'existe pas."));
            }

        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }

    public function createProd()
    {
        // Headers requis
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie la méthode
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // On instancie les produits
            $produit = new ProduitsModel();

            // On récupère les informations envoyées
            $donnees = json_decode(file_get_contents("php://input"));

            if (!empty($donnees->nom) && !empty($donnees->description) && !empty($donnees->prix) && !empty($donnees->categories_id)) {
                // Ici on a reçu les données
                // On hydrate notre objet
                $params = [':nom' => $donnees->nom,
                    ':description' => $donnees->description,
                    ':prix' => $donnees->prix,
                    ':categories_id' => $donnees->categories_id];

                if ($produit->create($params)) {
                    // Ici la création a fonctionné
                    // On envoie un code 201
                    http_response_code(201);
                    echo json_encode(["message" => "L'ajout a été effectué"]);
                } else {
                    // Ici la création n'a pas fonctionné
                    // On envoie un code 503
                    http_response_code(503);
                    echo json_encode(["message" => "L'ajout n'a pas été effectué"]);
                }
            }
        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }

    public function updateProd()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: PUT");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie la méthode
        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {

            // On instancie les produits
            $produit = new ProduitsModel();

            // On récupère les informations envoyées
            $donnees = json_decode(file_get_contents("php://input"));

            $id = "";
            $params = [];
            if (!empty($donnees->id)) {
                $id = $donnees->id;
            };
            if (!empty($donnees->nom)) {$params[':nom'] = $donnees->nom;};
            if (!empty($donnees->description)) {$params[':description'] = $donnees->description;};
            if (!empty($donnees->prix)) {$params[':prix'] = $donnees->prix;};
            if (!empty($donnees->categories_id)) {$params[':categories_id'] = $donnees->categories_id;};
            if (!empty($id) && !empty($params)) {
                // Ici on a reçu les données
                if ($produit->update($id, $params)) {
                    // Ici la modification a fonctionné
                    // On envoie un code 200
                    http_response_code(200);
                    echo json_encode(["message" => "La modification a été effectuée"]);
                } else {
                    // Ici la création n'a pas fonctionné
                    // On envoie un code 503
                    http_response_code(503);
                    echo json_encode(["message" => "La modification n'a pas été effectuée"]);
                }
            }
        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }

    public function deleteProd()
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: DELETE");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // On vérifie que la méthode utilisée est correcte
        if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {

            // On instancie les produits
            $produit = new ProduitsModel();

            // On récupère l'id du produit
            $donnees = json_decode(file_get_contents("php://input"));

            if (!empty($donnees->id)) {
                if ($produit->delete($donnees->id)) {
                    // Ici la suppression a fonctionné
                    // On envoie un code 200
                    http_response_code(200);
                    echo json_encode(["message" => "La suppression a été effectuée"]);
                } else {
                    // Ici la création n'a pas fonctionné
                    // On envoie un code 503
                    http_response_code(503);
                    echo json_encode(["message" => "La suppression n'a pas été effectuée"]);
                }
            }
        } else {
            // On gère l'erreur
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"]);
        }
    }
}