<?php
namespace App\Core;

class Router
{
    public $url;
    public $routes = [];

    public function __construct($url)
    {
        $this->url = trim($url, '/');
    }

    /**
     * get("/",UnController@index())
     *
     * @param  string $path
     * @param  string $action
     * @return void
     */
    public function get(string $path, string $action)
    {
        //on sauvegarde la route selon la methode GET,POST
        $this->routes['GET'][] = new Route($path, $action);
    }

    /**
     * post("/",UnController@index())
     *
     * @param  string $path
     * @param  string $action
     * @return void
     */
    public function post(string $path, string $action)
    {
        //on sauvegarde la route selon la methode GET,POST
        $this->routes['POST'][] = new Route($path, $action);
    }

    /**
     * put("/",UnController@index())
     *
     * @param  string $path
     * @param  string $action
     * @return void
     */
    public function put(string $path, string $action)
    {
        //on sauvegarde la route selon la methode GET,POST
        $this->routes['PUT'][] = new Route($path, $action);
    }

    /**
     * delete("/",UnController@index())
     *
     * @param  string $path
     * @param  string $action
     * @return void
     */
    public function delete(string $path, string $action)
    {
        //on sauvegarde la route selon la methode GET,POST
        $this->routes['DELETE'][] = new Route($path, $action);
    }

    public function run()
    {
        //On boucle sur les routes en fonction de la méthode qu'on récupère avec $_SERVER[REQUEST_METHOD]
        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
            //Si la route match, si elle existe
            if ($route->matches($this->url)) {
                //On appelle le bon controller avec la bonne fonction
                return $route->execute();
            };
        };
        //si rien ne match on retourne une 404
        return header('HTTP/1.0 404 Not Found');
    }
}