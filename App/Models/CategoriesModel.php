<?php
namespace App\Models;

class CategoriesModel extends Model
{

    // Propriétés
    protected $id;
    protected $nom;
    protected $description;
    protected $created_at;
    protected $updated_at;
    protected $produits_nom;
    protected $produits_id;
    protected $produits_desc;
    protected $produits_prix;

    /**
     * Constructeur
     *
     */

    public function __construct()
    {
        $this->table = "categories";
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get the value of updated_at
     */
    public function getUpdated_at()
    {
        return $this->updated_at;
    }

    /**
     * Set the value of updated_at
     *
     * @return  self
     */
    public function setUpdated_at($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of produits_nom
     */
    public function getProduits_nom()
    {
        return $this->produits_nom;
    }

    /**
     * Set the value of produits_nom
     *
     * @return  self
     */
    public function setProduits_nom($produits_nom)
    {
        $this->produits_nom = $produits_nom;

        return $this;
    }

    /**
     * Get the value of produits_id
     */
    public function getProduits_id()
    {
        return $this->produits_id;
    }

    /**
     * Set the value of produits_id
     *
     * @return  self
     */
    public function setProduits_id($produits_id)
    {
        $this->produits_id = $produits_id;

        return $this;
    }

    /**
     * Get the value of produits_desc
     */
    public function getProduits_desc()
    {
        return $this->produits_desc;
    }

    /**
     * Set the value of produits_desc
     *
     * @return  self
     */
    public function setProduits_desc($produits_desc)
    {
        $this->produits_desc = $produits_desc;

        return $this;
    }

    /**
     * Get the value of produits_prix
     */
    public function getProduits_prix()
    {
        return $this->produits_prix;
    }

    /**
     * Set the value of produits_prix
     *
     * @return  self
     */
    public function setProduits_prix($produits_prix)
    {
        $this->produits_prix = $produits_prix;

        return $this;
    }

    /**
     * findOneById
     *
     * @param  int $id
     */
    public function findAllProductByNameCategorie(string $name)
    {
        $name = ucfirst(htmlspecialchars(strip_tags($name)));
        $sql = "SELECT c.nom, c.id, p.nom as produits_nom, p.description as produits_desc, p.prix as produits_prix, p.id as produits_id
        FROM {$this->table} c
        LEFT JOIN produits p
        ON p.categories_id = c.id
        WHERE c.nom = :name";

        // On prépare la requête
        return $this->requete($sql, [':name' => $name]);
    }
}