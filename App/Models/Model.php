<?php
namespace App\Models;

use App\Core\Db;
use PDO;
use PDOStatement;

class Model extends Db
{
    // Table de la base de données
    protected $table;

    // Instance de connexion
    private $db;

    /**
     * requete
     *
     * @param  string $sql requete sql
     * @param  array $params tableau de paramètres
     * @return PDOStatement
     */
    public function requete(string $sql, array $params = null): PDOStatement
    {
        // On récupère l'instance de Db
        $this->db = Db::run();

        //on vérifie si params[] est null
        if ($params !== null) {
            //requete préparée
            //$sql="INSERT INTO personnes (prenom,nom) VALUES (:unPrenom,:unNom)";
            //$modele = $this->db-> prepare($sql);
            //$modele -> execute(array(':unPrenom' => 'Cémoi',':unNom' => 'Toto'));
            $modele = $this->db->prepare($sql);
            $modele->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
            $modele->execute($params);
            return $modele;
        } else {
            //requéte simple
            //$sql = "SELECT * FROM personnes";
            //$modele = $this->db->query($sql);
            $modele = $this->db->query($sql);
            $modele->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
            return $modele;
        }
    }

    /**
     * create
     *
     * @param  array $params tableau associatif [':key'=>'value']
     * @return PDOStatement
     */
    public function create(array $params): PDOStatement
    {
        $champs = [];
        $values = [];
        foreach ($params as $key => $param) {
            $champs[] = substr($key, 1, strlen($key));
            $values[] = $key;
        };
        $col = implode(',', $champs);
        $val = implode(',', $values);

        foreach ($params as $key => $value) {
            $params[$key] = htmlspecialchars(strip_tags($value));
        };

        $sql = "INSERT INTO {$this->table} ($col) VALUES($val)";
        return $this->requete($sql, $params);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM {$this->table}";
        return $this->requete($sql);
    }

    /**
     * update
     *
     * @param int $id id de la table
     * @param  array $params tableau associatif [':key'=>'value']
     * @return PDOStatement
     */
    public function update(int $id, array $params): PDOStatement
    {
        $champs = [];
        foreach ($params as $key => $param) {
            $champs[] = substr($key, 1, strlen($key)) . "=" . $key;
        };
        $col = implode(',', $champs);

        $params[':id'] = $id;
        foreach ($params as $key => $value) {
            $params[$key] = htmlspecialchars(strip_tags($value));
        };

        $sql = "UPDATE {$this->table} SET $col WHERE id=:id";
        return $this->requete($sql, $params);
    }

    /**
     * delete
     *
     * @param  int $id
     * @return void
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM {$this->table} WHERE id=:id";
        $params[':id'] = (int) htmlspecialchars(strip_tags($id));
        if ($this->requete($sql, $params)) {
            return true;
        }
        return false;
    }
}