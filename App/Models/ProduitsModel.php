<?php
namespace App\Models;

class ProduitsModel extends Model
{

    // Propriétés
    protected $id;
    protected $nom;
    protected $description;
    protected $prix;
    protected $categories_id;
    protected $categories_nom;
    protected $created_at;

    /**
     * Constructeur
     *
     */

    public function __construct()
    {
        $this->table = "produits";
        if (!is_null($this->categories_id)) {
            $sql = "SELECT nom FROM categories WHERE id=$this->categories_id";
            $this->categories_nom = $this->requete($sql)->fetch()->nom;
        }
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nom
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of prix
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of categories_id
     */
    public function getCategories_id()
    {
        return $this->categories_id;
    }

    /**
     * Set the value of categories_id
     *
     * @return  self
     */
    public function setCategories_id($categories_id)
    {
        $this->categories_id = $categories_id;

        return $this;
    }

    /**
     * Get the value of categories_nom
     */
    public function getCategories_nom()
    {
        return $this->categories_nom;
    }

    /**
     * Set the value of categories_nom
     *
     * @return  self
     */
    public function setCategories_nom($categories_nom)
    {
        $this->categories_nom = $categories_nom;

        return $this;
    }

    /**
     * Get the value of created_at
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     *
     * @return  self
     */
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * findOneById
     *
     * @param  int $id
     * @return void
     */
    public function findOneById(int $id): ?self
    {
        $sql = "SELECT c.nom as categories_nom, p.id, p.nom, p.description, p.prix, p.categories_id, p.created_at
        FROM {$this->table} p
        LEFT JOIN categories c
        ON p.categories_id = c.id
        WHERE p.id = :id
        LIMIT 0,1";

        // On prépare la requête
        $query = $this->requete($sql, [':id' => $id]);

        // on récupère la ligne
        $data = $query->fetch();
        return ($data) ? $data : null;
    }
}