CREATE DATABASE IF NOT EXISTS api_rest;
DROP TABLE IF EXISTS produits;
DROP TABLE IF EXISTS categories;
CREATE TABLE IF NOT EXISTS categories(
    id INTEGER NOT NULL AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
CREATE TABLE IF NOT EXISTS produits(
    id INTEGER NOT NULL AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    prix DECIMAL(10, 2),
    categories_id INTEGER NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    FOREIGN KEY(categories_id) REFERENCES categories (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
INSERT INTO `categories` (`nom`, `description`, `created_at`, `updated_at`)
VALUES (
        'Mode',
        'Catégorie pour tout ce qui est en rapport avec la mode.',
        '2019-06-01 00:32:07',
        '2019-08-30 15:34:33'
    ),
    (
        'Electronique',
        'Gadgets, drones et plus.',
        '2018-06-03 02:34:11',
        '2019-01-30 16:34:33'
    ),
    (
        'Moteurs',
        'Sports mécaniques',
        '2018-06-01 10:33:07',
        '2019-07-30 15:34:54'
    ),
    (
        'Films',
        'Produits cinématographiques.',
        '2018-06-01 10:33:07',
        '2018-01-08 12:27:26'
    ),
    (
        'Livres',
        'E-books, livres audio...',
        '2018-06-01 10:33:07',
        '2019-01-08 12:27:47'
    ),
    (
        'Sports',
        'Articles de sport.',
        '2018-01-09 02:24:24',
        '2019-01-09 00:24:24'
    );
INSERT INTO `produits` (
        `nom`,
        `description`,
        `prix`,
        `categories_id`,
        `created_at`,
        `updated_at`
    )
VALUES (
        'Samsung Galaxy S 10',
        'Le dernier né des téléphones Samsung',
        '899',
        2,
        '2019-09-07 21:19:09',
        '2019-09-07 19:19:09'
    ),
    (
        'Habemus Piratam',
        "Le livre à propos d'un pirate informatique",
        '6',
        6,
        '2019-09-07 21:21:11',
        '2019-09-07 19:21:11'
    );