<?php
use App\Autoloader;
use App\Core\Router;

define('ROOT', dirname(__DIR__));
define('VIEWS', ROOT . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']));

require '../autoloader.php';

Autoloader::register();

$url = isset($_GET['url']) ? $_GET['url'] : '';
$router = new Router($url);

$router->get('/produit/read', 'ProduitsController@readAll');
$router->get('/produit/read/:id', 'ProduitsController@readOne');
$router->post('/produit/create', 'ProduitsController@createProd');
$router->put('/produit/update', 'ProduitsController@updateProd');
$router->delete('/produit/delete', 'ProduitsController@deleteProd');

$router->get('/categorie/read', 'CategoriesController@readAll');
$router->get('/categorie/read/:name', 'CategoriesController@readOne');
$router->post('/categorie/create', 'CategoriesController@createProd');
$router->put('/categorie/update', 'CategoriesController@updateProd');
$router->delete('/categorie/delete', 'CategoriesController@deleteProd');

$router->run();